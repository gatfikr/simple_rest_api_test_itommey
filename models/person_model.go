package models

type Person struct {
	ID        int    `gorm:"primary_key" json:"id"`
	FirstName string `gorm:"type:varchar(50)" json:"first_name,omitempty" binding:"required"`
	LastName  string `gorm:"type:varchar(50)"json:"last_name,omitempty" binding:"required"`
}

func (p Person) TableName() string {
	return "tb_person"
}
