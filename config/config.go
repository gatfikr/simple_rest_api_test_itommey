package config

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
	"os"
	"simple_rest_api_test_itommey/models"
)

// config your database here..

var db *gorm.DB

func ConnectDB() *gorm.DB {
	conn := os.Getenv("POSTGRES_BRI")
	db, err := gorm.Open("postgres", conn)

	if err != nil {
		fmt.Println("[CONFIG.ConnectDB] Error when connect to DB")
		log.Fatal(err)
	}

	models.InitTable(db)

	return db
}
