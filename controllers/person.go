package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"net/http"
	"simple_rest_api_test_itommey/models"
)

type UserController struct {
	DB *gorm.DB
}

// code your controllers to database here...

func (u *UserController) GetPerson(c *gin.Context) {
	var (
		person models.Person
		result gin.H
	)
	id := c.Param("id")
	err := u.DB.Where("id = ?", id).First(&person).Error
	if err != nil {
		result = gin.H{
			"result": err.Error(),
			"count":  0,
		}
	} else {
		result = gin.H{
			"result": person,
			"count":  1,
		}
	}

	c.JSON(http.StatusOK, result)
}

// get all data in person
func (u *UserController) GetPersons(c *gin.Context) {
	message := "success"
	status := "0000"
	var (
		persons []models.Person
		result  gin.H
	)

	u.DB.Find(&persons)
	if len(persons) <= 0 {
		result = gin.H{
			"result": nil,
			"count":  0,
		}
	} else {
		result = gin.H{
			"status":   status,
			"messages": message,
			"data":     persons,
		}
	}

	c.JSON(http.StatusOK, result)
}

// create new data to the database
func (u *UserController) CreatePerson(c *gin.Context) {
	//var (
	//	person models.Person
	//	result gin.H
	//)
	//first_name := c.PostForm("first_name")
	//last_name := c.PostForm("last_name")
	//person.FirstName = first_name
	//person.LastName = last_name
	//u.DB.Create(&person)
	//result = gin.H{
	//	"status": "0000",
	//	"result": "success",
	//}
	//c.JSON(http.StatusOK, result)

	//db := c.MustGet("db").(*gorm.DB)
	var input models.Person
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	person := models.Person{
		FirstName: input.FirstName,
		LastName:  input.LastName,
	}

	u.DB.Create(&person)

	c.JSON(http.StatusOK, gin.H{
		"status":   "0000",
		"messages": "success",
		//"data": person,
	})

}

// update data with {id} as query
func (u *UserController) UpdatePerson(c *gin.Context) {
	var person models.Person
	err := u.DB.Where("id = ?", c.Param("id")).First(&person).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var input models.Person
	if 	err := c.ShouldBindJSON(&input);err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	u.DB.Model(&person).Updates(input)

	c.JSON(http.StatusOK, gin.H{
		"data": person,
	})
	//id := c.Query("id")
	//first_name := c.PostForm("first_name")
	//last_name := c.PostForm("last_name")
	//var (
	//	person    models.Person
	//	newPerson models.Person
	//	result    gin.H
	//)
	//
	//err := u.DB.First(&person, id).Error
	//if err != nil {
	//	result = gin.H{
	//		"result": "data not found",
	//	}
	//}
	//newPerson.FirstName = first_name
	//newPerson.LastName = last_name
	//err = u.DB.Model(&person).Updates(newPerson).Error
	//if err != nil {
	//	result = gin.H{
	//		"result": "update failed",
	//	}
	//} else {
	//	result = gin.H{
	//		"result": "successfully updated data",
	//	}
	//}
	//c.JSON(http.StatusOK, result)
}

// delete data with {id}
func (u *UserController) DeletePerson(c *gin.Context) {
	var (
		result gin.H
		person models.Person
	)
	id := c.Param("id")
	err := u.DB.First(&person, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	err = u.DB.Delete(&person).Error
	if err != nil {
		result = gin.H{
			"result": "delete failed",
		}
	} else {
		result = gin.H{
			"status": "0000",
			"result": "Data deleted successfully",
		}
	}

	c.JSON(http.StatusOK, result)
}
