# README #

### What is this repository for? ###

This repo is for making REST API sample programs using golang.

The following steps must be done:

1.   Pull the master branch to your local
2.   Create a new branch with the format: *test-rest-golang-nama* e.g. *test-rest-golang-fendy*
3.   Create a simple REST API program using the Golang language using a database (MySQL, PostgreSQL, etc).
4.   The REST API created contains CURD commands to the database.
5.   It is recommended to use the gin-gonic library
6.   After all tasks are finished, push your branch to bitbucket

### Database ###

```sql
CREATE TABLE IF NOT EXISTS `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL DEFAULT '0',
  `last_name` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
)
```

### Spesification ###

#### CREATE PERSON #####

##### REQUEST #####

```shell
curl -X POST 'localhost/api/user/create' \
  -d '{
    "first_name": "Bambang",
    "last_name": "Pamungkas",
    }'
```

##### RESPONSE #####

```shell
'{
    "status":"0000",
    "message":"Success"
}'
```

#### GET PERSON #####

```shell
curl -X GET 'localhost/api/user/getall' 
```

##### RESPONSE #####

```shell
'{
    "status":"0000",
    "message":"Success",
    "Data":[
        {
            "id":1,
            "first_name":"Bambang",
            "last_name":"Pamungkas",
        },
    ]
}'
```

#### UPDATE PERSON #####

```shell
curl -X PATCH 'localhost/api/user/update' \
  -d '{
    "id": 1
    "first_name": "Bambang",
    "last_name": "Pranoto",
    }'
```

##### RESPONSE #####

```shell
'{
    "status":"0000",
    "message":"Success",
}'
```

#### DELETE PERSON #####

```shell
curl -X DELETE 'localhost/api/user/delete/{id}'
```

##### RESPONSE #####

```shell
'{
    "status":"0000",
    "message":"Success",
}'
```
