package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/subosito/gotenv"
	"simple_rest_api_test_itommey/controllers"
	"simple_rest_api_test_itommey/config"
)

var db *gorm.DB

func init()  {
	gotenv.Load()
}

func main() {
	fmt.Println("Hello World!!")

	//port := os.Getenv("PORT_BRI")
	db := config.ConnectDB()
	inDB := &controllers.UserController{DB:db}
	defer db.Close()

	router := gin.Default()

	//router.Use(func(c *gin.Context) {
	//	c.Set("db", db)
	//	c.Next()
	//})

	router.GET("/person/:id", inDB.GetPerson)
	router.GET("/persons", inDB.GetPersons)
	router.POST("/person", inDB.CreatePerson)
	router.PATCH("/persons/updates/:id", inDB.UpdatePerson)
	router.DELETE("/person/:id", inDB.DeletePerson)
	router.Run(":9000")


}
