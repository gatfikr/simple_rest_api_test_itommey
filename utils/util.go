package utils

import (
	"encoding/json"
	"fmt"
	"net/http"
	"simple_rest_api_test_itommey/models"
)

func HandleSuccess(resp http.ResponseWriter, status int, data interface{}) {
	response := models.Response{
		Status:  "0000",
		Message: "Success",
		Data:    data,
	}
	resp.Header().Set("Content-Type", "application/json")
	resp.WriteHeader(status)

	err := json.NewEncoder(resp).Encode(response)
	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		resp.Write([]byte("Ooops, something error"))
		fmt.Printf("[HandleSuccess] Error when encode data with error: %v\n", err)
	}
}

func HandleError(resp http.ResponseWriter, status int, msg string) {
	response := models.Response{
		Status:  "0000",
		Message: msg,
	}
	resp.Header().Set("Content-Type", "application/json")
	resp.WriteHeader(status)

	err := json.NewEncoder(resp).Encode(response)
	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		resp.Write([]byte("Ooops, something error"))
		fmt.Printf("[HandleSuccess] Error when encode data with error: %v\n", err)
	}
}
